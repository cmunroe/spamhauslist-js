"use strict";
const r = require('axios');
var list;

function update(){
    return new Promise((resolve, reject) => {
        const asdrop = r.get('https://www.spamhaus.org/drop/asndrop.txt');
        Promise.all([asdrop])
        .then(values => {
            var data;
            values.forEach(value => {
                data += value.data;
    
            });
            return data;
        })
        .then(values => {
            return values.split("\n");
        })
        .then(values => {
            var data = [];
            values.forEach(value =>{
                
                value = value.split(";")[0];
                value = value.replace("AS", "");
                value = value.trim();
                value = parseInt(value);
                if(!isNaN(value)){data.push(value)}
                
    
            })
            return data;
        })
        .then(values => {
            list = values;
            resolve();
        })
        .catch(errors => {
            reject(errors);
        })

    })

}

function check(asn){
    asn = parseInt(asn);
    if(list){
        return list.includes(asn);
    }
    else{
        return null;
    }
}


update();
setInterval(update, 1000 * 60 * 60 * 24);
module.exports = {check, update};